﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.Linq;
using System.Web;


public class UniversalBag : DynamicObject
{
    IDictionary<string, object> prop;
    NameValueCollection prop2;
    //HttpApplicationState prop2;

    public UniversalBag(IDictionary<string, object> outerBag)
    {
        this.prop = outerBag;

        
    }

    public UniversalBag(NameValueCollection outerBag)
    {
        this.prop2 = outerBag;
    }

    public override bool TrySetMember(SetMemberBinder binder, object value)
    {
        if (prop != null) prop[binder.Name] = value;
        if (prop2 != null) prop[binder.Name] = value;

        return true;
    }

    public override bool TryGetMember(GetMemberBinder binder, out object result)
    {
        if (prop != null ) result = prop[binder.Name];
        else result = prop2[binder.Name];
        return true;
    }
}
