﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace StateManagementDemo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Application["number"] = 0;
            Application["started"] = DateTime.Now.ToLongTimeString();
        }

        protected void Session_Start()
        {
            Session["number"] = 0;
            Session["started"] = DateTime.Now.ToLongTimeString();
        }
    }
}
