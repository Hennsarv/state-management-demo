﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StateManagementDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Test = "Henn Sarv";
            ViewBag.Number = 4;
            ViewData["Test"] = "Sarviktaat";
            ViewData["Number"] = 7;



            return View();
        }

        public ActionResult Test(int? id, int? comm)
        {
            ViewBag.id = id ?? -1;
            ViewBag.comm = comm ?? 0;

            HttpCookie cookie = new HttpCookie("test");
            if (comm.HasValue)
            {
                cookie.Values["comm"] = comm.ToString();
                cookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(cookie);
            }

            cookie = Request.Cookies["test"];

            string fromCookie = cookie?.Values["comm"] ?? "puudub";
            ViewBag.fromCookie = fromCookie;


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}