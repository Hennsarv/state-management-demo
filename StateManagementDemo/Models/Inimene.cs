﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StateManagementDemo.Models
{
    public class Inimene
    {
        public int Id;
        public string Nimi;
        public int Vanus;

        public static List<Inimene> KõikInimesed = new List<Inimene>();

        public Inimene()
        {
            KõikInimesed.Add(this);
        }

        public List<Inimene> Inimesed
        {
            get => Inimene.KõikInimesed;
        }
    }
}